import os, sys, shutil

class Launch_by_choice(object):
	def __init__(self):
		self.items_={}

	def addChoiceItem(self, key, caption, param):
		self.items_[str(key)]={'caption': caption, 'param': param}
		#print (self.items_.keys())

	def items(self):
		return self.items_;

	def getAnswer(self):
		for key,value in self.items_.items():
			caption = value['caption']
			print(f'[{key}]: {caption}')
		answer = input()
		if answer not in self.items_.keys():
			print('Ответ не корректный')
			print()
			return self.getAnswer()		
		return answer

	def launch(self,func, title, exitAnswer='0'):
		answer = None
		while answer != exitAnswer:
			print(title)
			answer = self.getAnswer();			
			item = self.items_.get(answer,None)
			if (item is not None) and ('param' in item.keys()):
				param = item['param']
				func(param, self.items_.items())


def getAnswerByDic(title, choices, disableKeys=[]):
	''' получения ключа от пользователя из словаря '''
	for key,value in choices.items():
		if not(key in disableKeys):
			print(f'[{key}]: {value[0]}')
	try:
		answer = int(input())
	except ValueError:
		print('не корректный ввод')
		answer = 0
	if not answer in choices.keys():
		print ('Ответ не корректный')
		print ()
		answer = getAnswerByDic(title, dict, disableKeys)
	return answer

def runByAnswer(title, choices, func, exitAnswer=0, disableKeys=[]):	
	answer = ''
	while answer != exitAnswer:				
		print()
		print(title)		
		answer = getAnswerByDic(title, choices, disableKeys) #получаем ключ по словарю
		if answer in choices.keys() and not(answer in disableKeys):
			param = choices[answer][1]
			func(param,disableKeys) #выполняем функцию c параметром
		else:
			print('Некорректный ввод.')


def execLoadRepo(params, username, password):
	title = params['title']
	path = params['path']
	cmd = params['cmd'].format(username=username, password=password)
	copy_to = getValByKey(params,'copy_to','')
	branch = getValByKey(params,'branch','')
	print(f'начало загрузки {title}')
	if not os.path.exists(path): # если нет директории
		os.makedirs(path) # cоздаем директорию
	else:
		print('Каталог этого проекта существует.')
		print('Т.к. нужен чистый каталог то существующий будет переименован')
		ind=0
		while os.path.exists(path+str(ind)):
			ind += 1
		os.rename(path,path+str(ind))
	cmd = f'{cmd} {path}'
	os.system(cmd) # клонирование репозитория в созданную директорию

	if branch != '' :
		print(f'Переход на ветку: {branch}')
		cmd = f'cd {path} && git checkout {branch}'
		os.system(cmd) # переход на другую ветку

	paths = path.split('/')
	lastDir = ''
	while lastDir == '':
		lastDir = paths.pop();
	smallPath = '/'.join(paths)
	copyFolder(f'{smallPath}/copyto{lastDir}/', f'{path}/')
	#cmd=f'cp -rv {smallPath}/copyto{lastDir}/* {path}/'
	#print(cmd)
	#os.system(cmd)
	if copy_to != '':
		copyFolder(f'{path}/', f'{copy_to}/')
	#	copyFolder(path, copy_to)

def screеnClear(): # очистка экрана
	if sys.platform  == 'linux' or platform == linux2:
		os.system('clear')
	if sys.platform == 'darwin':
		os.system('clear')
	if sys.platform == 'win32':
		os.system('cls')

def copyFolder(sourceFolder,receiverFolder,title=''): # рекурсивное копирование папки
	if title == '':
		print(f'Copy folder from "{sourceFolder}*" to "{receiverFolder}"')
	else:
		print(title)
	#shutil.copy(sourceFolder+'/*.*', receiverFolder)
	cmd = f'shopt -s dotglob && cp -rvfp {sourceFolder}* {receiverFolder}'
	print(cmd)
	os.system(cmd)

def buildImage(params):
	title = params['title']
	docker = params['dockername']
	cmd = params['cmd']
	print('Build '+title)
	if docker != '':
		# удаляем контейнер
		print('удаляем старый контейнер')
		com=f'docker rm dockers_{docker}_1' 
		print(com)
		os.system(com)
		# удаляем образ
		print('удаляем старый образ')
		com=f'docker rmi dockers_{docker}'
		print(com)
		os.system(com)
		# создаем образ
		print('Создаем образ')
		com=f'docker-compose build {docker}'
		print(com)
		os.system(com)
	else:
		# выполняем команду
		com=f'{cmd}'
		print(com)
		os.system(com)

def getValByKey(dic,key,defVal):
	if key in dic.keys():
		return dic[key]
	else:
		return defVal

def changeText(text, params):		
	if params is not None:
		newText = []		
		for tstr in text:
			add_str=[]
			for param in params:
				search = getValByKey(param,'search',None)
				replace = getValByKey(param,'replace',None)
				insert_before = getValByKey(param,'insert_before',None)
				insert_after = getValByKey(param,'insert_after',None)
				if search is not None:
					pos = tstr.find(search)
					if pos>-1:
						if insert_before is not None:
							newText.append(insert_before+'\n')
							print(f"before '{search}' -> '{insert_before}'")
						if insert_after is not None:
							add_str.append(insert_after+'\n')
							print(f"after '{search}' -> '{insert_after}'")
						if replace is not None:
							tstr=tstr.replace(search,replace)
							print(f"replace '{search}' -> '{replace}' = '{tstr}' ")

			newText.append(tstr)
			newText.extend(add_str)
		return newText
	else:
		return text
	
def printa(arr):
	n=0
	for lines in arr:
		print(f'{n} {lines}')
		n += 1

def changeConf(params):
	for param in params: # для каждого элемента массива
		file_name = getValByKey(param,'file',None) # получим параметры
		data = getValByKey(param,'data',None)
		if not(file_name is None) or os.path.exists(file_name): # если файл существует
			with open(file_name, 'r') as file: # откроем его
				print (f'Open file {file_name} ')
				text = file.readlines()				
				text = changeText(text, data) # поменяем текст по правилам
			split_fn=file_name.split('/')
			split_fn[len(split_fn)-1]='confBack_'+split_fn[len(split_fn)-1]
			new_file_name='/'.join(split_fn)
			if not os.path.exists(f'{new_file_name}'):
				os.rename(file_name,f'{new_file_name}') #переименовываем файл
				with open(file_name, 'w') as file: # откроем с созданием новый файл со старым именем
					#printa(text)
					file.writelines(text) # запишем новые данные в файл
					print (f'saved file {file_name} ')
			else:
				print ('This file is already modified')
		else:
			print(f'Error: File -> "{file_name}" is not exists!')

def runCommand(array_cmd, param): # выполняет несколько комманд
	for cmd in array_cmd:
		if param != None:
			fullcmd = cmd.format(input = param)
		else:
			fullcmd = cmd
		print(fullcmd)
		os.system(fullcmd)

