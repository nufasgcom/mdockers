#: -*- coding: utf-8 -*-
########################
#:   LOCAL SETTINGS   :#
########################
import os


DEBUG = True

ALLOWED_HOSTS = ['127.0.0.1', 'localhost', 'put_ip_here', '192.168.18.29']

BASE_URL = '/teleskz/'

DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.mysql',
		'NAME': 'newskz',
		'USER': 'root',
		'PASSWORD': '123456',
		'HOST': 'db',
		'PORT': '3306',
		'OPTIONS': {
			"init_command": "SET foreign_key_checks = 0;",
		},
	}
}

CORS_ORIGIN_ALLOW_ALL = DEBUG
ORS_ALLOW_CREDENTIALS = DEBUG

#: session lifetime, неделя
SESSION_COOKIE_AGE = 3600

STATIC_URL = '/teleskz/static/'
STATIC_FOLDER = '/teleskz/static/'
FILESDIR = os.path.abspath(os.path.dirname(__file__))
MEDIA_ROOT = os.path.join(FILESDIR, '..', 'static/media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/static/media/'

CACHES = {
	'default': {
		'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
	}
}