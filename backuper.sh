#!/bin/bash
# скрипт выполнения полного бакапа логического тома (LV)

# Раздел для задания переменных

# имя docker контейнера с сервером БД для остановки на время создания snapshot
DBDOCKER="dockers_db_1" 
# размер SnapShot например 500М - это в мегабайтах , 33G - это в гигабайтах
SIZE="400M"
# имя виртальной группы (VG) в формате /dev/group 
VG="/dev/sql"
# имя тома с которого будем снимать snapShot
DATA_VOLUME="data"
# имя тома на котором будет хранится snapShot
SNAP_VOLUME="snpdata"
# путь для монтирования snapshot
SNAP_PATH="/mnt/snpdata"
# путь для записи backup
BACKUP_PATH="/usr/backup"
# дата-время backup в формате ГГГГММДД ЧЧММСС 20200630_101036 
DT=$(date +"%Y%m%d_%H%M%S")
# режим backup : 1 - копированием; 2 - архивированием
MODE="2"
# подпапка для сохранения логов если " " - то вывод на экран
LOG="log"
EMPTY=""
# конец раздел для задания переменных

function dt(){
	dati=$(date +"%d.%m.%Y %H:%M:%S")
	echo $dati - 
}


echo -------------------------------------------------
echo -------------------------------------------------
echo -------------------------------------------------

# создаем каталог для backup
if ! [ -d $BACKUP_PATH ]; then
	echo $(dt) - создаем каталог для backup
	echo $(dt) - mkdir $BACKUP_PATH
	mkdir $BACKUP_PATH
fi

if [[ $LOG == $EMPTY ]];  then
	OUT='/dev/stdout'
	EOUT='/dev/stdout'
else
	if ! [ -d $BACKUP_PATH/$LOG ]; then
 		echo $(dt) создаем каталог для логов
		echo $(dt) mkdir $BACKUP_PATH/$LOG
		mkdir $BACKUP_PATH/$LOG
	fi
	OUT="$BACKUP_PATH/$LOG/$DT.log"
	EOUT="$BACKUP_PATH/$LOG/$DT""_ERROR.log"
	echo $(dt) лог смотрите в $OUT
	echo $(dt) ошибки смотрите в $EOUT
fi

echo $(dt) backUp запущен >>$OUT
if [ "$LOG"!="" ];  then
	echo $(dt) backUp запущен
fi

# останавливаем контейнер с БД если он задан
if [ "$DBDOCKER"!="$EMPTY" ]; then
	echo $(dt) docker stop $DBDOCKER >>$OUT
	docker stop $DBDOCKER >>$OUT 2>>$EOUT
fi

# код для выполнения
# создание snapshot
echo $(dt) lvcreate -L $SIZE -s -n $SNAP_VOLUME $VG/$DATA_VOLUME >>$OUT
lvcreate -L $SIZE -s -n $SNAP_VOLUME $VG/$DATA_VOLUME >>$OUT 2>>$EOUT

# запускаем контейнер с БД если он задан
if [ "$DBDOCKER"!="$EMPTY" ]; then
	echo $(dt) docker start $DBDOCKER >>$OUT
	docker start $DBDOCKER >>$OUT 2>>$EOUT
fi

# создаем каталог для подкдючения snapshot
if ! [ -d $SNAP_PATH ]; then
	echo $(dt) создаем каталог для подключения snapshot >>$OUT
	echo $(dt) mkdir $SNAP_PATH >>$OUT
	mkdir $SNAP_PATH >>$OUT 2>>$EOUT
fi
echo $(dt) монтируем snapshot в каталог >>$OUT
echo $(dt) mount $VG/$SNAP_VOLUME $SNAP_PATH >>$OUT
mount $VG/$SNAP_VOLUME $SNAP_PATH >>$OUT 2>>$EOUT
# если backkup копированием то создаем подкаталог
if [ "$MODE" -eq "1" ];  then
	if ! [ -d $BACKUP_PATH/backup_$DT ]; then
		echo $(dt) создаем подкаталог для backup >>$OUT
		echo $(dt) mkdir $BACKUP_PATH/backup_$DT >>$OUT
		mkdir $BACKUP_PATH/backup_$DT >>$OUT 2>>$EOUT
	fi
	# вариант 1 - 
	echo $(dt) backup копированием >>$OUT
	echo $(dt) cp -rfv $SNAP_PATH/* $BACKUP_PATH/backup_$DT/ >>$OUT
	cp -r $SNAP_PATH/* $BACKUP_PATH/backup_$DT/ >>$OUT 2>>$EOUT
else
	# вариант 2 - 
	echo $(dt) backup архивированием >>$OUT
	echo $(dt) tar -zcvf $BACKUP_PATH/backup_$DT.tar $SNAP_PATH/ >>$OUT
	tar -zcvf $BACKUP_PATH/backup_$DT $SNAP_PATH >>$OUT 2>>$EOUT
fi
echo $(dt) размонтируем snapshot из каталога >>$OUT
echo $(dt) umount $SNAP_PATH >>$OUT
umount $SNAP_PATH >>$OUT 2>>$EOUT
echo $(dt) удаляем snapshot >>$OUT
echo $(dt) lvremove $VG/$SNAP_VOLUME >>$OUT
lvremove $VG/$SNAP_VOLUME -y >>$OUT 2>>$EOUT
echo $(dt) backUp окончен >>$OUT
if [ "$LOG"!="" ];  then
	echo $(dt) backUp окончен
fi