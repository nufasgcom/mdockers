import os, sys, mytools

mainAnswers = {
	'0':'Загрузить репозитории и данные',
	'1':'Изменить настройки',
	'2':'Cоздать образы',
	'3':'Экспортировать образы',
	'4':'Импортировать образы',
	'9':'Выход'
}

otherAnswers = {
	'0':'Все',
	'1':'ГРП',
	'2':'СКЗ',
	'3':'Messenger',
	'4':'Posthandler',
	'5':'Notifier',
	'6':'Daemon',
	'7':'MySQL',
	'9':'Выход',
}

def main(): # основная функция
	runByAnswer('Выберите действие:', mainAnswers, mainRun, '9')

def runByAnswer(title, answersDict, runDict, exitAnswer) :
	os.system('clear');
	print()
	print(title)
	print()
	answer = ''
	while answer != exitAnswer :
		print()
		answer = mytools.getAnswerByDic(title, answersDict) #получаем ключ по словарю
		if answer in answersDict.keys():
			runDict[answer]() #выполняем функцию по ключу
		else:
			print('Некорректный ввод.')

def mainExit():
	print()

def loadRepo(): # загрузка даннных
	def allRepo():
		print('All')
	def grpRepo():
		print('grp')
	def skzRepo():
		print('skz')
	def messengerRepo():
		print()
	def posthandlerRepo():
		print()
	def notifierRepo():
		print()
	def daemonRepo():
		print()
	def mySQLData():
		print()


	loadRepos = {
	'0':allRepo,
	'1':grpRepo,
	'2':skzRepo,
	'3':messengerRepo,
	'4':posthandlerRepo,
	'5':notifierRepo,
	'6':daemonRepo,
	'7':mySQLData,
	'9':mainExit,	
	}


	runByAnswer(mainAnswers['0'], otherAnswers, loadRepos, '9')

def changeSettings():
	print('изменить настройки')


def createImage():
	print('Create')
def exportImage():
	print('Export')
def importImage():
	print('Import')

mainRun = {
	'0':loadRepo,
	'1':changeSettings,
	'2':createImage,
	'3':exportImage,
	'4':importImage,
	'9':mainExit
}	

if __name__ == '__main__':
	main()