# после первого запуска rabbit нужно создать пользователей

# шаблон для добавления адимистратора
# docker exec rabbit rabbitmqctl add_user josuelima MyPassword999
# docker exec rabbit rabbitmqctl set_user_tags josuelima administrator
docker exec dockers_rabbit_1 rabbitmqctl add_user root toor
docker exec dockers_rabbit_1 rabbitmqctl set_user_tags root administrator
docker exec dockers_rabbit_1 rabbitmqctl set_permissions -p / root ".*" ".*" ".*"

# шаблон для добавления пользователя приложения
# docker exec rabbit rabbitmqctl add_user ruby_app SuperPassword000
# docker exec rabbit rabbitmqctl set_permissions -p / ruby_app ".*" ".*" ".*"


# для безопасности рекомендуют удалить гостевого пользователя
# но в нашем случае по моему не стоит
#docker exec rabbit rabbitmqctl delete_user guest
