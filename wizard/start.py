import os, sys, mytools, datetime
from inspect import isfunction

username = 'nufasgcom'
password = 'G0gitech'

ALL = 'all'
EXIT = 'exit'

#/////////////////////////////////////////////////////////////////////////////////////////////////
def fDisk():# работа с диском - томами
	def choiceFunc(selectedParam,allChoices):			
		dic = selectedParam
		if isinstance(dic,dict):
			line= None
			if 'input_title'  in dic.keys():					
				line = input(dic['input_title'])
			if 'cmd' in dic.keys():
				mytools.runCommand(dic['cmd'],line) #запуск команды или команд
			else:
				print ('Что то пошло не так')					
		elif dic is None:
			print ('Возврат к предыдущему меню')
		else:
			print ('Что то пошло не так')


	mytools.screеnClear()
	choices = mytools.Launch_by_choice()
	choices.addChoiceItem('1','Просмотр всех разделов дисков (fdisk -l)', {	'cmd':['sudo fdisk -l']})
	choices.addChoiceItem('2','Просмотр всех разделов выбранного диска (fdisk -l)', {	'cmd':		['sudo fdisk -l {input}'], 
																			'input_title':['Укажите диск : ']
																		})
	choices.addChoiceItem('3','Осторожно  - Удаление раздела (fdisk d)',{	'cmd':		['sudo fdisk {input}'], 
																			'input_title':['Укажите диск, а после в диалоге введите "d" и далее по подсказкам, и в конце для сохранения введите "w" :']
																		})
	choices.addChoiceItem('4','Cоздание раздела (fdisk n)',{	'cmd':		['sudo fdisk {input}'], 
																			'input_title':['Укажите диск, а после в диалоге введите "n" и далее по подсказкам, и в конце для сохранения введите "w" :']
																		})
	choices.addChoiceItem('5','Перевести идентификатор раздела в LVM (fdisk t)',{	'cmd':		['sudo fdisk {input}'], 
																			'input_title':['Укажите диск, а после в диалоге введите "t" и далее введите "8e" (Linux LVM), и в конце для сохранения введтие "w" :']
																		})
	choices.addChoiceItem('6','Инициализация физического тома PV (pvcreate)',{	'cmd':		['sudo pvcreate {input}'], 
																			'input_title':['Укажите раздел PV :']
																		})
	choices.addChoiceItem('7','Cоздание группы томов vgcreate {VG PV1 PV2 ..}',{	'cmd':		['sudo vgcreate {input}'], 
																			'input_title':['Укажите имя группы и через пробелы имена физических томов : ']
																		})
	choices.addChoiceItem('8','Cоздание логического тома (необходимо оставить место под snapshot) lvcreate (-L{size}G -n {lv} {vg}) ',{	'cmd':		['sudo lvcreate {input}'], 
																			'input_title':['Укажите по шаблону (-L{size}G -n {lv} {vg}) где size - размер в Гб, lv - имя тома, vg - имя группы; например -l50G mydata datagroup : ']
																		})
	choices.addChoiceItem('9','Форматирование в системе ext4 ',{	'cmd':		['sudo mkfs.ext4 {input}'], 
																			'input_title':['Введите полный путь к логическому тому; ']
																		})
	choices.addChoiceItem('0','Вернуться на уровень выше',None)
	choices.launch(choiceFunc,'Выберите действие:')	




#/////////////////////////////////////////////////////////////////////////////////////////////////
def loadCode(): # загрузка даннных

	def loadData(param):
		global username
		global password
		title = param['title']
		print()
		print()
		print()
		print(title)
		print(f'Задайте имя пользователя: ({username})')
		var = input();
		if var != '' :
			username = var;
		print('Введите пароль: ('+'*'*len(password)+')')
		var = input();
		if var != '' : 
			password = var		
		mytools.execLoadRepo(param, username, password)

	def choiceFunc(selectedParam,allChoices):				
		dic = selectedParam

		if isinstance(dic, dict):
			loadData(dic)
		elif dic == ALL:
			for key, value in allChoices:
				idic = value.get('param',None)
				if isinstance(idic, dict):
					choiceFunc(idic,allChoices)
		elif dic is None:
			print ('Возврат к предыдущему меню')
		else:
			print ('Что то пошло не так')


	#////// begin BODY loadCode
	mytools.screеnClear()
	print('Перед созданием образом рекомендуется проверить настройки.')
	choices = mytools.Launch_by_choice()
	choices.addChoiceItem('1','Всё *',			ALL)
	choices.addChoiceItem('2','ГРП', 			{'title' : 'ГРП', 				'path' : 'web/code/grp', 		'cmd' : 'git clone http://{username}:{password}@bitbucket.org/usgteam/grp.git'})
	choices.addChoiceItem('3','СКЗ', 			{'title' : 'СКЗ', 				'path' : 'web/code/teleskz', 	'cmd' : 'git clone https://{username}:{password}@bitbucket.org/usgteam/teleskz.git',		'branch':'dockers'})
	choices.addChoiceItem('4','Messenger', 		{'title' : 'Messenger', 		'path' : 'messenger/code', 		'cmd' : 'git clone https://{username}:{password}@bitbucket.org/usgteam/messenger.git'})
	choices.addChoiceItem('5','Posthandler',	{'title' : 'Posthandler', 		'path' : 'posthandler/code',	'cmd' : 'git clone https://{username}:{password}@bitbucket.org/usgteam/posthandler.git'})
	choices.addChoiceItem('6','Notifier', 		{'title' : 'Notifier', 			'path' : 'notifier/code', 		'cmd' : 'git clone https://{username}:{password}@bitbucket.org/usgteam/notifier.git',		'branch':'dev'})
	choices.addChoiceItem('7','GOSTEncryption', {'title' : 'Шифрование ГОСТ', 	'path' : 'messenger/gost', 		'cmd' : 'git clone https://{username}:{password}@bitbucket.org/usgteam/gostencryption.git', 'copy_to':'messenger/code'})
	choices.addChoiceItem('0','Вернуться на уровень выше',None)
	choices.launch(choiceFunc,'Выберите действие:')	
	#////// end BODY loadCode

#////////////////////////////////////////////////////////////////////////////////
def createImage():
	def  choiceFunc(selectedParam,allChoices):
		dic = selectedParam
		if isinstance(dic, dict):
			mytools.buildImage(dic)
		elif dic == ALL:
			for key, value in allChoices:
				par = value.get('param',None)
				if isinstance(par, dict):
					choiceFunc(par, allChoices)
		elif dic is None:
			print ('Возврат к предыдущему меню')
		else:
			print ('Что то пошло не так')

	#////// begin BODY CREATE IMAGE
	mytools.screеnClear()
	print('Перед созданием образом рекомендуется проверить настройки.')
	choices = mytools.Launch_by_choice()	
	choices.addChoiceItem('1','Всё *',				ALL)
	choices.addChoiceItem('2','Web (ГРП + СКЗ)', 	{'title':'Web', 'dockername':'web', 'cmd':'' })
	choices.addChoiceItem('3','Messenger', 			{'title':'Messenger', 'dockername':'messenger', 'cmd':''})
	choices.addChoiceItem('4','Posthandler',		{'title':'Posthandler', 'dockername':'posthandler', 'cmd':''})
	choices.addChoiceItem('5','Notifier',			{'title':'Notifier', 'dockername':'notifier', 'cmd':''})
	choices.addChoiceItem('6','Celery beat',		{'title':'Celery_beat', 'dockername':'celery_beat', 'cmd':''})
	choices.addChoiceItem('7','Celery worker',		{'title':'Celery_worker', 'dockername':'celery_worker', 'cmd':''}),
	choices.addChoiceItem('8','Daemon',				{'title':'Daemon', 'dockername':'daemon1', 'cmd':''})
	choices.addChoiceItem('0','Вернуться на уровень выше',None)
	choices.launch(choiceFunc,'Выберите действие:')
	#////// end BODY CREATE IMAGE

#/////////////////////////////////////////////////////////////////////////////////////////////////
def replaceConfigs():
	params = {
		'grp':
			[
				{	'file':'web/code/grp/system.conf.php',
					'data': [
						{'search':'$dbhost="127.0.0.1";', 'replace':'$dbhost="db";'},
						{'search':'$dbuser="grp";', 'replace':'$dbuser="root";'},
						{'search':'$dbpass="uhg";', 'replace':'$dbpass="123456";'},
						{'search':'$cfgServer = "127.0.0.1";', 'replace':'$cfgServer = "messenger";'},
					]
				}
			],
		'skz':
			[
				{	'file':'web/code/teleskz/teleskz/settings.py',
					'data': [
						{'search':"CACHE_PREFIX_SKZ = 'skz'", 'replace':"CACHE_PREFIX_SKZ = 'teleskz'"},
						{'search':"#: CELERY SETTINGS   :#", 'insert_before':"#####################"},
						{'search':"#: CELERY SETTINGS   :#", 'insert_before':"#: RABBIT SETTINGS :#"},
						{'search':"#: CELERY SETTINGS   :#", 'insert_before':"#####################"},
						{'search':"#: CELERY SETTINGS   :#", 'insert_before':"RABBIT_HOST = 'rabbit'"},
						{'search':"#: CELERY SETTINGS   :#", 'insert_before':"RABBIT_USER = 'root'"}, 
						{'search':"#: CELERY SETTINGS   :#", 'insert_before':"RABBIT_PASSWORD = 'toor'"},
						{'search':"#: CELERY SETTINGS   :#", 'insert_before':"RABBIT_PORT = 5672"},
						{'search':"#: CELERY SETTINGS   :#", 'insert_before':"RABBIT_VHOST = ''"},
						{'search':"#: CELERY SETTINGS   :#", 'insert_before':""},
						{'search':"#: CELERY SETTINGS   :#", 'insert_before':"####################"},
					]
				},
				{	'file':'web/code/teleskz/daemon.py',
					'data': [
						{'search':'parameters = pika.ConnectionParameters()', 'replace':'parameters = pika.ConnectionParameters(host=cfg.RABBIT_HOST)'},
					]
				},
			],
		'messenger':
			[
				{	'file':'messenger/code/app/config.py',
					'data':[
						{'search':"RABBITMQ_HOST = 'localhost'", 'replace':"RABBITMQ_HOST = 'rabbit'"},
					]
				},
			],
		'notifier':
			[
				{	'file':'notifier/code/app/config.py', 
					'data':[
						{'search':"RABBITMQ_HOST = 'localhost'", 'replace':"RABBITMQ_HOST = 'rabbit'"},
					]
				},
			],
		'posthandler':
			[
				{	'file':'posthandler/code/bus.py',
					'data':[
						{'search':'parameters = pika.ConnectionParameters()', 'replace':'parameters = pika.ConnectionParameters(host=config.RABBITMQ_HOST)'},
					]
				},
				{	'file':'posthandler/code/config.py',
					'data':[
						{'search':"DB_USER = ''", 'replace':"DB_USER = 'root'"},
					]
				},
				{	'file':'posthandler/code/config.py', 
					'data': [
						{'search':"DB_PASS = ''", 'replace':"DB_PASS = '123456'"},
						{'search':"DB_NAME = ''", 'replace':"DB_NAME = 'grp'"},
						{'search':"DB_HOST = '127.0.0.1'", 'replace':"DB_HOST = 'db'"},
						{'search':"DB_HOST = '127.0.0.1'", 'insert_after':"RABBITMQ_HOST='rabbit'"},
					]
				},
			],
	}

	def choiceFunc(selectedParam,allChoices):
		param = selectedParam
		if isinstance(param, list):
			mytools.changeConf(param)
		elif param == ALL:
			for key, value in allChoices:
				par = value.get('param',None)
				if isinstance(par, list):
					choiceFunc(par, allChoices)
		elif param is None:
			print ('Возврат к предыдущему меню')
		else:
			print ('Что то пошло не так')
			

	#///// begin BODY relaceConfigs
	print()
	print('Изменение конфигурации:')	
	choices = mytools.Launch_by_choice()
	choices.addChoiceItem('1','Все*', ALL)
	choices.addChoiceItem('2','ГРП', params['grp'])
	choices.addChoiceItem('3','СКЗ, Daemon, Celery', params['skz'])
	choices.addChoiceItem('4','Messenger',  params['messenger'])
	choices.addChoiceItem('5','Posthandler', params['posthandler'])
	choices.addChoiceItem('6','Notifier',  params['notifier'])
	choices.addChoiceItem('0','Вернуться на уровень выше',  None)
	choices.launch(choiceFunc,'Выберите действие:')
	#///// end BODY relaceConfigs

#////////////////////////////////////////////////////////////////////////////////////////////////	

def main(): # основная функция
	def choiceFunc(selectedParam,allChoices): # функция выполняемая с параметрами выбранного пункта
		func = selectedParam
		if func is not None:
			func()

	gchoices = mytools.Launch_by_choice()
	gchoices.addChoiceItem('1','Разметка диска под sql_data', fDisk)
	gchoices.addChoiceItem('2','Загрузить репозитории и данные', loadCode)
	gchoices.addChoiceItem('3','Изменить конфигурации', replaceConfigs)
	gchoices.addChoiceItem('4','Cоздать образы', createImage)
	gchoices.addChoiceItem('0','Выход', None)
	gchoices.launch(choiceFunc,'Выберите действие:')

if __name__ == '__main__':
	main()